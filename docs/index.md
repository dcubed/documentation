# D Cubed Documentation
This is the documentation for the D Cubed collection of libraries.
Inside you will find tutorials and design rationale.
The API reference is kept elsewhere.